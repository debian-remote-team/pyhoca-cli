pyhoca-cli (0.6.1.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 0001_pyhoca-cli-frontend.py-Fix-reference-before-assignme.patch.
      Applied upstream.
  * debian/control:
    + Assure presence of python-x2go >= 0.6.1.4.
  * debian/copyright:
    + Update copyright attribution for debian/.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 13 Feb 2024 17:37:35 +0100

pyhoca-cli (0.6.1.2-2) unstable; urgency=medium

  * debian/patches:
    + Add 0001_pyhoca-cli-frontend.py-Fix-reference-before-assignme.patch. Fix
      reference for assignment issue in resume_session() method.
  * debian/control:
    + Bump Standards-Version: to 4.5.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 11 Mar 2020 01:58:52 +0100

pyhoca-cli (0.6.1.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/upstream/signing-key.asc:
    + Update upstream PGP pubkey (0xF4A7678C9C6B0B2B).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 27 Dec 2019 12:20:55 +0100

pyhoca-cli (0.6.1.1-1) unstable; urgency=medium

  * New upstream release.
    - Add X2Go Kdrive support.
  * debian/control:
    + Add Rules-Requires-Root: field and set it to no.
    + Bump Standards-Version: to 4.4.1. No changes needed.
  * debian/upstream/signing-key.asc:
    + Fix lintian issue public-upstream-key-not-minimal.
  * debian/control:
    + Bump DH compat level to version 12.
    + Update versioned D (pyhoca-cli): python3-x2go (>= 0.6.1.0).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 23 Nov 2019 23:27:45 +0100

pyhoca-cli (0.6.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Update versioned D (pyhoca-cli) on python-x2go: Now, at least version
      0.6.0.2 is required.
    + Fix wrong version in B-D on python3-all.
    + Use secure URL in Homepage: field.
  * debian/compat:
    + Drop file. Use new debhelper-compat B-D in control file instead.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 03 Dec 2018 10:30:27 +0100

pyhoca-cli (0.6.0.0-1) unstable; urgency=medium

  * New upstream release.
    - Ported to Python3.
  * debian/*: White-space clean-up.
  * debian/control:
    + New team ML address: debian-remote@lists.debian.org.
    + Update Vcs-*: fields. Packaging Git has been migrated to
      salsa.debian.org.
    + Bump Standards-Version: to 4.2.1. No changes needed.
    + Add B-D: dh-python.
  * debian/{control,compat}: Bump DH compat level to version 11.
  * debian/rules:
    + Adapt dh command call to Python3 build using pybuild.
    + Ignore failures on dh_auto_clean.
  * debian/copyright:
    + Use secure URL in Source: field.
    + Use secure URI for copyright format reference.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.
  * debian/watch:
    + Use secure URL to obtain upstream orig tarball.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 19 Sep 2018 15:12:18 +0200

pyhoca-cli (0.5.0.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: field to 4.1.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 25 Sep 2017 15:42:03 +0200

pyhoca-cli (0.5.0.3-2) unstable; urgency=medium

  * debian/control:
    + New maintenance umbrella: Debian Remote Maintainers team.
    + Bump Standards-Version: to 3.9.8. No changes needed.
  * debian/copyright: Update years in Copyright: fields.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 30 Nov 2016 13:21:56 +0100

pyhoca-cli (0.5.0.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/upstream/signing-key.asc:
    + Add public Key for Key-Id 0x9C6B0B2B (X2Go Git Administrator
      <git-admin@x2go.org>).
  * debian/control:
    + Bump Standards: to 3.9.7. No changes needed.
    + Update Section: field. Change from "python" to "x11". (Closes: #820379).
    + Use encrypted URLs in VCS-*: fields.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 12 Apr 2016 12:47:22 +0200

pyhoca-cli (0.5.0.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/rules:
    + Add get-orig-source rule.
  * debian/copyright:
    + Update copyright years.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 16 Jun 2015 12:34:06 +0200

pyhoca-cli (0.5.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Don't use subfolder x2go/ on Alioth's collab-maint anymore.
    + Raise versioned D (pyhoca-cli): python-x2go (>= 0.5.0.0).
    + Bump Standards: to 3.9.6. No changes needed.
    + Add to S (pyhoca-gui): mteleplayer-clientside (not yet in Debian).
    + Replace "LDAP support" by "session brokerage support" in LONG_DESCRIPTION.
  * debian/upstream/changelog:
    + Stop manually shipping upstream ChangeLog files anymore. From now on
      provided in upstream tarball.
  * debian/copyright:
    + Update copyright years.
  * debian/rules:
    + Update copyright date in file header.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Oct 2014 09:23:54 +0200

pyhoca-cli (0.4.0.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch:
    + Add cryptographic signature verification for upstream tarball.
  * debian/control:
    + Use my debian.org mail address in Uploaders: field.
    + Bump Standards: to 3.9.5. No changes needed.
    + Alioth-canonicalize Vcs-Git: field.
  * Update upstream changelog.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 06 Aug 2014 17:50:02 +0200

pyhoca-cli (0.4.0.1-1) unstable; urgency=low

  * New upstream release.
  * /debian/control:
    + Drop build dependency on python-x2go due to a change in setup.py when
      retrieving the egg version.
    + Versioned Depend: python-x2go (>= 0.4.0.2-0~).
    + Bump Standards: to 3.9.4, no changes needed.
  * /debian/copyright:
    + Place packaging files under AGPL-3+ license, so that the packaging
      license is identical to the upstream license.
  * Raise compat level to 9. No changes needed.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Mon, 04 Mar 2013 05:33:57 +0100

pyhoca-cli (0.2.1.0-2) unstable; urgency=low

  * Re-upload package (no changes) due to faulty changelog header in
    uploaded revison 0.2.1.0-1 in Debian's Incoming queue.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Mon, 10 Dec 2012 23:44:10 +0100

pyhoca-cli (0.2.1.0-1) unstable; urgency=low

  * New upstream version.
  * /debian/control:
    + Depend on python-x2go (>= 0.2.1.0-0~).

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Mon, 10 Dec 2012 23:42:57 +0100

pyhoca-cli (0.2.0.4-1) unstable; urgency=low

  * New upstream release. (Closes: #684734).
  * /debian/control:
    + Versioned Build-Depends on python (>= 2.6.6-14~).
    + Add ${python:Depends} to Depends.
    + Fix typo (corrected: bandwidth) in long description.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Mon, 20 Aug 2012 09:08:31 +0200

pyhoca-cli (0.2.0.3-1) unstable; urgency=low

  * New upstream release.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Tue, 24 Jul 2012 22:22:40 +0200

pyhoca-cli (0.2.0.2-1) unstable; urgency=low

  * Initial package build (Closes: #655607).

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Mon, 23 Jul 2012 21:49:36 +0200
